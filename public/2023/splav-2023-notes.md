# Poznámky k splavu 2023

## Všeobecné

Splav sa konal od 22.07.2023 do 25.07.2023.
Bolo nás 10, mali sme požičané 4 plastové kanojky a jednu vlastnú nafukovaciu kanojku.

Logy z GPSky sú v priečinku `gpx`. Sú tiež nahodené na [mapy.cz](https://en.mapy.cz/s/foponezepo).

## Prvý deň

Z lodenice sme vyrážali o 11:30.
Časť po Dunaji bola úplne bez problémov, nefúkalo, ani neboli také veľké vlny.
Je fajn si zobrať so sebou do lode rozumne dostupné jedlo a vodu,
keďže do Čunova je to pomerne ďaleko.

Prenáška v Čunove je pomerne dlhá, je fajn si ju nechať na niečo viac k večeru,
aby sa neprenášalo cez to najväčšie teplo.

[Miesto na kempovanie na jazerách](https://en.mapy.cz/s/cofuhedoma) je fajn,
dá sa tam založiť oheň a je tam pokoj, aj keď cez deň (a víkend) tam zvyknú chodiť ľudia,
najmä z kempu.

## Druhý deň

Druhý deň sme sa ráno kúpali v jazerách a skákali z [hojdačky.](https://en.mapy.cz/s/pevopenehu)
Od jazier sme odchádzali až 12:45.

V [kempe](https://en.mapy.cz/s/cegadomaza) sme boli okolo 14:00,
dali sme si tam pizzu, dá sa tam nabrať voda (iba teplá), ale normálne sme ju pili a varili z nej.

Z kempu sme odchádzali okolo 16:00, cesta bola bez problémov,
[toto rameno](https://en.mapy.cz/s/folovovaba) bolo zapadané stromami,
ale dalo sa to bez problémov obísť cez [vedľajšie rameno](https://en.mapy.cz/s/napegoseso).

[V tomto mieste](https://en.mapy.cz/s/nacajareze) sa časť ľudí odpojila od hlavného toku,
a išli cez [vedľajšie ramená](https://en.mapy.cz/s/cadutuvuko).
Boli tam popadané stromy, ale dali sa buď obísť alebo jednoducho nadplávať.

Ďalej sme sa tiež rozdelili. Časť išla cez [hať](https://en.mapy.cz/s/lubozobefu)
a časť to obišla [týmto bočným ramenom](https://en.mapy.cz/s/bujeracufa).
Obe cesty boli bez problémov.

Na konci nás čakala ešte [jedna hať](https://en.mapy.cz/s/lulehebaro).
Nesplavovali sme ju, aj keď zopár ľudí nad tým premýšľalo.
Prenáška bola úplne fajn, stačilo iba preniesť lode krížom cez hať,
a aj miesto na naloženie a vyloženie bolo fajn.

Prvý nápad bol spať [na tomto ostrove](https://en.mapy.cz/s/dehucanebo),
ale nebolo tam dosť miesta na stany.

Nakoniec sme spali [na tomto ostrove](https://en.mapy.cz/s/kugeratodo),
a vyloďovali sa v brode. Je tam pomerne silný prúd a šmýka sa,
tak na to treba byť nachystaní.

## Tretí deň

Na vodu sme sa dostali okolo 11:00.
Počas oboplávavania ostrova sme si povedali,
že to vyskúšame obísť cez [toto rameno](https://en.mapy.cz/s/mezococufe),
ale v tomto mieste v ňom bol spadnutý strom, cez ktorý sme lode museli prenášať.

Obedovali sme pri [tejto hati](https://en.mapy.cz/s/mezococufe).
Väčšina lodí ju nakoniec splavila, niektoré aj s vecami.

Nasledovali dve celkom akčné hate, [prvá](https://en.mapy.cz/s/nobubupehe)
sa dá splaviť aj stredom, ale do lodí sa nám naliala voda.

[Druhú hať](https://en.mapy.cz/s/hejumukeju) sme nakoniec splavili všetci,
jedna loď aj s vecami.
Plastové lode ju splavovali ľavým krajom, gumená loď stredom.
Pravý kraj nie je splavný, keďže sú tam veľké kamene.

Netáborili sme [na mieste vyznačenom na mapách](https://en.mapy.cz/s/resalovuva),
keďže sme chceli ísť ešte trochu ďalej, a boli tam akurát nejakí ľudia.

[Prvá hať](https://en.mapy.cz/s/desapekuzu) bola splavná,
ale [druhá](https://en.mapy.cz/s/haduvajoru) už nie,
takže sme ju obchádzali bočnými ramenami cez [túto hať](https://en.mapy.cz/s/jomavuhaza),
ktorá bola splavná bez problémov.

Utáborili sme sa [tu](https://en.mapy.cz/s/lorodukode). Veľa miesta na stany tam nebolo.
Večer nám prišli povedať ujovia, že nemáme byť pod stromami,
kúpať sa (kvôli búrke) a zakladať oheň.

## Štvrtý deň

Ráno sme začali splavením [hate](https://en.mapy.cz/s/kolajemebe) pod miestom na spanie,
bolo to bez problémov aj s vecami.

Vyrazili sme okolo 9:30, ale po chvíli sa rozpršalo,
takže sme sa [tu](https://en.mapy.cz/s/cudenakeje) vylodili
a približne 2 hodiny čakali kým doprší.

[Táto hať](https://en.mapy.cz/s/fujopetoga) bola nesplavná,
ale prenáška bola krátka - stačilo iba preniesť lode a veci krížom.

Potom sme sa už len pripojili k Dunaji a odpadovému kanálu z Gabčíkova.

Za Sapom boli na zopár miestach na ľavej strane celkom vysoko trčiace kamene z vody.

Pôvodný plán na [preplavenie sa do Medveďova priamo](https://en.mapy.cz/s/metevosotu) nefungoval,
keďže bolo málo vody, takže sme zašli [za most](https://en.mapy.cz/s/numejugope),
a potom sa chvíľu vracali.